const express = require("express");
const controllers = require("../app/controllers");
const multer = require("multer");
const apiRouter = express.Router();
const path = require("path");
const { requireAuth, checkUser } = require("../app/middlewares/authMiddleware");
const { authorizeAdmin } = require("../app/middlewares/roleMiddleware");

/**
 * Authentication Resource
 * */

const diskStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__basedir, "../", "/public"));
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
  },
});
const uploadFile = multer({ storage: diskStorage });

// user
apiRouter.get("/user/home", controllers.api.v1.pengguna.home);
apiRouter.post("/user/register", controllers.api.v1.pengguna.register);
apiRouter.get("/admin/addForm", authorizeAdmin, checkUser, controllers.api.v1.cars.formAddAdmin);
apiRouter.post("/admin/add", authorizeAdmin, checkUser, controllers.api.v1.cars.addAdmin);
// apiRouter.post("/admin/add", controllers.api.v1.pengguna.register);
apiRouter.post("/user/login", controllers.api.v1.pengguna.login);
apiRouter.get("/user/findCar", requireAuth, checkUser, controllers.api.v1.pengguna.findCar);
apiRouter.get("/user/logout", controllers.api.v1.pengguna.logout);
// end user

// end auth  

apiRouter.get("/short/:id/cari", authorizeAdmin, checkUser, controllers.api.v1.cars.short)
apiRouter.get("/", authorizeAdmin, checkUser, controllers.api.v1.cars.list)
apiRouter.get("/cars", authorizeAdmin, checkUser, controllers.api.v1.cars.list)
// GET /cars/create dari file create.ejs
apiRouter.get("/cars/formAdd", authorizeAdmin, checkUser, controllers.api.v1.cars.formAdd);
//Create a new car
apiRouter.post("/cars/add", authorizeAdmin, checkUser, uploadFile.single("photo"), controllers.api.v1.cars.add);
apiRouter.get("/cars/:id/update", authorizeAdmin, checkUser, controllers.api.v1.cars.selectCar);
// update a car
apiRouter.post("/cars/update/:id", authorizeAdmin, checkUser, uploadFile.single("photo"), controllers.api.v1.cars.updateCar);
// Delete car by ID
apiRouter.get('/cars/delete/:id', authorizeAdmin, checkUser, controllers.api.v1.cars.deleteCar);
// apiRouter.get(
//   "/api/v1/whoami",
//   controllers.api.v1.authController.authorize,
//   controllers.api.v1.authController.whoAmI
// );
// apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);
// apiRouter.post("/api/v1/register", controllers.api.v1.authController.register);

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
