// const { updateCar } = require("../controllers/api/v1/carsController");
const carsRepository = require("../repositories/carsRepositories");

module.exports = {
    async short({ size_id }) {
        console.log("coba lihat size_id", size_id)
        try {
            const shorting = await carsRepository.short({ size_id });
            return shorting;
        } catch (err) {
            throw err;
        }
    },

    async list() {
        try {
            const cars = await carsRepository.findAll();
            console.log("car", cars);
            return cars;
        } catch (err) {
            throw err;
        }
    },

    async listSize() {
        try {
            const size = await carsRepository.findAllSize();
            console.log("sere", size);
            return size;
        } catch (err) {
            throw err;
        }
    },

    create(requestBody) {
        return carsRepository.create(requestBody);
    },

    createAdmin(requestBody) {
        return carsRepository.createAdmin(requestBody);
    },

    async oneCar({ id }) {
        try {
            const cars = await carsRepository.findOneCar({ id });
            // const coba = await carsRepository.findAllUkur();
            console.log("cs", cars)
            return cars;
        } catch (err) {
            throw err;
        }
    },

    async updateCar({ id }, requestBody) {
        // console.log("service id", id);
        try {
            const update = carsRepository.updateCar(id, requestBody);
            return update;
        }
        catch (err) {
            throw err;
        }
    },

    async deleteCar({ id }) {
        console.log("coba lihat id service", id)
        try {
            const del = await carsRepository.deleteCar({ id });
            return del;
        } catch (error) {
            throw err;
        }

    }

};
