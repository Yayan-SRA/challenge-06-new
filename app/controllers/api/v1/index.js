/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const authController = require("./authController");
const users = require("./penggunaController");
const cars = require("./carsController");
const pengguna = require("./penggunaController");

module.exports = {
  authController, users, cars, pengguna
};
