const carsService = require("../../../services/carsServices");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const SALT = 10;

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }
            resolve(encryptedPassword);
        });
    });
}
module.exports = {
    short(req, res) {
        const size_id = req.params.id;
        console.log("coba lihat id", size_id)
        carsService.short({ size_id }).then(cars => {
            const token = req.cookies.jwt;
            const pengguna = req.user;
            res.render("index", { cars, pengguna, token })
        })
    },

    list(req, res) {
        carsService
            .list()
            .then((cars) => {
                const token = req.cookies.jwt;
                console.log("coba", token);
                console.log("cobacobas", req.user);
                const pengguna = req.user;
                console.log("dahlah", pengguna.role_id);
                console.log(cars)
                res.render("index", { cars, pengguna, token });
            });
    },

    formAddAdmin(req, res) {
        const token = req.cookies.jwt;
        const pengguna = req.user;
        res.render("addAdmin", { pengguna, token })
    },
    async addAdmin(req, res) {
        // const candidate = await Pengguna.findOne({
        //     where: {
        //         email: req.body.email
        //     }
        // })

        // if (candidate) {
        //     res.status(409).json({
        //         message: 'This email is already taken. Try another.'
        //     })
        // } else {
        console.log(await req.body.role_id);
        carsService.createAdmin({
            role_id: req.body.role_id,
            name: req.body.name,
            email: req.body.email,
            password: await encryptPassword(req.body.password)
        })
            .then((pengguna) => {
                // res.render("/", { pengguna })
                res.send(
                    '<script>window.location.href="/";document.getElementById("alert-save").click();</script>'
                );
            }).catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            })
        // }
    },

    formAdd(req, res) {
        carsService
            .listSize()
            .then((size) => {
                const token = req.cookies.jwt;
                const pengguna = req.user;
                console.log("list ukur", size)
                res.render("addcar", { size, pengguna, token });
            });
    },

    add(req, res) {
        carsService
            .create({
                size_id: req.body.size_id,
                name: req.body.name,
                year: req.body.year,
                transmission: req.body.transmission,
                rentPerDay: req.body.rentPerDay,
                passenger: req.body.passenger,
                desc: req.body.desc,
                photo: req.file.filename,
            })
            .then((car) => {
                console.log(car);
                res.send(
                    '<script>window.location.href="/";document.getElementById("alert-save").click();</script>'
                );
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    async selectCar(req, res) {
        const token = req.cookies.jwt;
        const pengguna = req.user;
        const id = req.params.id;
        console.log("lihat id", id)
        const cars = await carsService.oneCar({
            id
        })
        console.log("lihat cc", cars);
        // console.log("get id", req.params.id);
        // console.log("hasil id", id);

        const coba = await carsService.listSize()
        console.log("coba", coba)
        console.log("cars", cars)
        res.render("edit", { cars, coba, pengguna, token });
    },

    updateCar(req, res) {
        const id = req.params;
        carsService.updateCar({ id }, {
            size_id: req.body.size_id,
            name: req.body.name,
            year: req.body.year,
            transmission: req.body.transmission,
            rentPerDay: req.body.rentPerDay,
            passenger: req.body.passenger,
            desc: req.body.desc,
            photo: req.file.filename,
        }).then(() => {
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't update car")
        })
    },

    deleteCar(req, res) {
        const id = req.params.id;
        console.log("coba lihat id", id)
        carsService.deleteCar({ id }).then(() => {
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't delete car")
        })
    }

};