const { Pengguna } = require("../../../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// const { User } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }
            resolve(encryptedPassword);
        });
    });
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(isPasswordCorrect);
        });
    });
}

function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
    home(req, res) {
        res.render("users/index",)
    },
    findCar(req, res) {
        const token = req.cookies.jwt;
        console.log("coba", token);
        console.log("cobacoba", req.user);
        const pengguna = req.user;
        res.render("users/carimobil", { pengguna, token })
    },

    async register(req, res) {
        const candidate = await Pengguna.findOne({
            where: {
                email: req.body.email
            }
        })

        if (candidate) {
            res.status(409).json({
                message: 'This email is already taken. Try another.'
            })
        } else {
            console.log(await req.body.role_id);
            Pengguna.create({
                role_id: req.body.role_id,
                name: req.body.name,
                email: req.body.email,
                password: await encryptPassword(req.body.password)
            })
                .then((pengguna) => {
                    res.render("users/index")
                    // res.status(201).json({
                    //     id: pengguna.id,
                    //     role_id: pengguna.role_id,
                    //     name: pengguna.name,
                    //     email: pengguna.email,
                    //     password: pengguna.password,
                    //     createdAt: pengguna.createdAt,
                    //     updatedAt: pengguna.updatedAt,
                    // })
                }).catch((err) => {
                    res.status(422).json({
                        status: "FAIL",
                        message: err.message,
                    });
                })
        }
    },

    async login(req, res, next) {
        const email = req.body.email.toLowerCase(); // Biar case insensitive
        const password = req.body.password;

        const pengguna = await Pengguna.findOne({
            where: { email },
        });

        if (!pengguna) {
            res.status(404).json({ message: "Email tidak ditemukan" });
            return;
        }

        const isPasswordCorrect = await checkPassword(
            pengguna.password,
            password
        );

        if (!isPasswordCorrect) {
            res.status(401).json({ message: "Password salah!" });
            return;
        }

        const token = createToken({
            id: pengguna.id,
            role_id: pengguna.role_id,
            name: pengguna.name,
            email: pengguna.email,
            createdAt: pengguna.createdAt,
            updatedAt: pengguna.updatedAt,
        });

        res.cookie("jwt", token, { httpOnly: true });
        if (token) {
            jwt.verify(
                token,
                process.env.JWT_SIGNATURE_KEY || "Rahasia",
                (err, decodedToken) => {
                    if (err) {
                        console.log(err, message);
                        res.redirect("/user/home");
                    } else {
                        console.log(decodedToken);
                        const role = decodedToken.role_id;
                        if (role == "1") {
                            res.redirect("/user/findCar");
                        } else if (role == "2" || role == "3") {
                            res.redirect("/");
                        }
                    }
                }
            );
        } else {
            res.status(422).json({
                status: "FAIL",
                message: err.message,
            });
        }

        // res.status(201).json({
        //     id: pengguna.id,
        //     role_id: pengguna.role_id,
        //     name: pengguna.name,
        //     email: pengguna.email,
        //     token,
        //     createdAt: pengguna.createdAt,
        //     updatedAt: pengguna.updatedAt,
        // });
        // res.redirect("/auth", { token });
        // next(token);
        // if (pengguna.role_id == "1") {
        //     res.render("users/index")
        // } else if (pengguna.role_id == "2" || pengguna.role_id == "3") {
        //     res.redirect("/")
        // }

    },

    // async whoAmI(req, res) {
    //     const usersaatini = req.user;
    //     console.log(usersaatini.id);
    //     if (usersaatini.id == "1") {
    //         res.render("users/index")
    //     }
    //     else if (usersaatini.id == "2" || usersaatini.id == "3") {
    //         res.redirect("/")
    //     }
    //     // res.status(200).json((usersaatini.id));
    // },

    async authorize(req, res, next) {
        try {
            console.log(req.headers.authorization)
            const bearerToken = req.headers.authorization;
            console.log(bearerToken);

            const token = bearerToken.split("Bearer ")[1];
            console.log(token);

            const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Rahasia");
            const id = tokenPayload.id

            req.pengguna = await Pengguna.findOne({
                where: { id },
            });

            next();

        }
        catch (err) {
            res.status(401).json({
                message: "Unauthorized"
            })
        }
    },

    async whoAmI(req, res) {
        if (req.Pengguna.role_id === 3) {
            res.status(200).json({
                status: "OK",
                data: req.user,
                message: "You are a member",
            });
        } else if (req.Pengguna.role_id === 2) {
            res.status(200).json({
                status: "OK",
                data: req.user,
                message: "You are an admin",
            });
        } else if (req.Pengguna.role_id === 1) {
            res.status(200).json({
                status: "OK",
                data: req.user,
                message: "You are a super admin",
            });
        }
    },

    async logout(req, res) {
        res.cookie("jwt", "", { maxAge: 1 });
        console.log("tes");
        res.redirect("/user/home");
    },
}