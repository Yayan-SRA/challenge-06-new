const jwt = require("jsonwebtoken");


const authorizeAdmin = async (req, res, next) => {
  try {
    const token = req.cookies.jwt;
    //cek token
    if (token) {
      jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia",
        (err, decodedToken) => {
          if (err) {
            console.log(err, message);
            res.redirect("/user/home");
          } else {
            const role = decodedToken.role_id;
            if (role == "3" || role == "2") {
              next();
            } else {
              res.redirect("/user/home");
            }
          }
        }
      );
    }
  } catch (err) {
    console.error(err);
    res.status(401).json({
      message: "Unauthorized",
    });
  }
}

module.exports = { authorizeAdmin };