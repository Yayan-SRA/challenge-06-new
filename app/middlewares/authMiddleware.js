const jwt = require("jsonwebtoken");
const { Pengguna } = require("../models");

const requireAuth = (req, res, next) => {
  const token = req.cookies.jwt;
  // check json web token exists & is verified
  if (token) {
    jwt.verify(
      token,
      process.env.JWT_SIGNATURE_KEY || "Rahasia",
      (err, decodedToken) => {
        if (err) {
          console.log(err.message);
          res.redirect("/user/home");
        } else {
          console.log("coba", decodedToken);
          next();
        }
      }
    );
  } else {
    res.redirect("/user/home");
  }
};

// check current user
const checkUser = async (req, res, next) => {
  try {
    const token = req.cookies.jwt;
    console.log("hmm", token);
    // const token = bearerToken.split("Bearer ")[1];
    // console.log(token);

    const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Rahasia");

    const id = tokenPayload.id

    req.user = await Pengguna.findOne({
      where: { id },
    });

    next();

  }
  catch (err) {
    res.status(401).json({
      message: "Unauthorized"
    })
  }


};

// // check current user
// const checkUser = (req, res, next) => {
//   const token = req.cookies.jwt;
//   console.log(token);
//   if (token) {
//     jwt.verify(token, process.env.JWT_SIGNATURE_KEY, async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;
//         console.log("tes", res.locals.user)
//         next();
//       } else {
//         let pengguna = await Pengguna.findById(decodedToken.id);
//         res.locals.user = pengguna;
//         console.log("tes1", res.locals.user)
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     console.log("tes2", res.locals.user)
//     next();
//   }
// };

module.exports = { requireAuth, checkUser };
