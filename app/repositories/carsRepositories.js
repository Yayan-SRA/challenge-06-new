const { Car, carSize, Pengguna } = require("../models");

module.exports = {
    short({ size_id }) {
        console.log("coba lihat size_id", size_id)
        return Car.findAll({
            order: ['id'],
            where: { size_id }
        });
    },
    findAll() {
        const mobil = Car.findAll({
            order: ['id']
        });
        console.log("mob", mobil)
        return mobil
    },
    findAllSize() {
        const coba = carSize.findAll({
            order: ['id']
        });
        console.log("repo", coba)
        return coba;

    },
    create(createBody) {
        return Car.create(createBody);
    },
    createAdmin(createBody) {
        return Pengguna.create(createBody);
    },
    findOneCar({ id }) {
        return Car.findOne({
            where: { id: id },
            include: [{
                model: carSize
            }]
        });
    },
    updateCar({ id }, updateBody) {
        return Car.update(updateBody, { where: { id } });
    },
    deleteCar({ id }) {
        console.log("coba lihat id repo", id)
        const hapus = Car.destroy({ where: { id } })
        console.log("coba lihat hapus", hapus)
        return hapus;
    }
};