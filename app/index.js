const express = require("express");
const morgan = require("morgan");
const path = require("path");
const router = require("../config/routes");
const app = express();
const cookieParser = require("cookie-parser");

app.use(cookieParser());
/** Install request logger */
app.use(morgan("dev"));

router.use(express.static(__dirname + "./public/"))

app.use(express.json())

// Path ke directory public
// Yang bakal kita jadikan public
// Sehingga user bisa akses CSS dan Javascript
// Di browser
// const PUBLIC_DIRECTORY = path.join(__dirname, "./public");

// Set format request
app.use(express.urlencoded({ extended: true }));

// Set PUBLIC_DIRECTORY sebagai
// static files di express
// app.use(express.static(PUBLIC_DIRECTORY));
// app.use('/public/upload', express.static(__dirname + '/public/upload'))

const PUBLIC_DIRECTORY = path.join(__dirname, "../", "./public");
// console.log(base_path);
app.use(express.static(PUBLIC_DIRECTORY));
console.log(PUBLIC_DIRECTORY);

// Bilang ke express kalo kita mau
// pake EJS sebagai view engine
app.set("view engine", "ejs");

// GET /car
// app.get("/", (req, res) => {
//     res.render("index");
// });

// coba up foto
global.__basedir = __dirname;
// const fs = require('fs');
// const fsPromises = require('fs').promises;
// const multer = require("multer");

/** Install Router */
app.use(router);

module.exports = app;
