'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      userRole.hasMany(models.Pengguna, {
        as: 'Pengguna',
        foreignKey: 'role_id'
      })
    }
  }
  userRole.init({
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userRole',
  });
  return userRole;
};