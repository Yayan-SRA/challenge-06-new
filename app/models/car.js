'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Car.belongsTo(models.carSize, {
        foreignKey: 'size_id'
      })
    }
  }
  Car.init({
    size_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    transmission: DataTypes.STRING,
    rentPerDay: DataTypes.INTEGER,
    passenger: DataTypes.STRING,
    year: DataTypes.INTEGER,
    desc: DataTypes.TEXT,
    photo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Car',
  });
  return Car;
};