'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class carSize extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      carSize.hasMany(models.Car, {
        as: 'Car',
        foreignKey: 'size_id'
      })
    }
  }
  carSize.init({
    size: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'carSize',
  });
  return carSize;
};